package main

import (
	"gitea.com/arydev/atriaBot/cmd"
	"github.com/NicoNex/echotron/v2"

	"fmt"
	"log"
)

const token = "1604831709:AAEvuzo1I1k_e6AS9FqQ_pPmMqaeAVf4H-I"

type bot struct {
	echotron.API
	chatId int64
}

func start(chatId int64) echotron.Bot {
	return &bot{
		echotron.NewAPI(token),
		chatId,
	}
}

func (b *bot) Update(update *echotron.Update) {
	msg := cmd.Parse(update)

	if msg != "" {
		b.SendMessage(cmd.Parse(update), b.chatId)
		log.Println("[info]:Command Parsed")
	}

}

func main() {
	dsp := echotron.NewDispatcher(token, start)
	fmt.Println("Bot Started!")
	log.Println(dsp.Poll())
}
