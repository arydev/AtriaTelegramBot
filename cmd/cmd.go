package cmd

import (
	"github.com/NicoNex/echotron/v2"
	"strings"
)

//Parse parses and executes a command
func Parse(update *echotron.Update) string {
	if update != nil {
		msg := strings.Split(update.Message.Text, " ")
		switch msg[0] {
		case "/start":
			return "Started"
		case "@Atria12bot":
			return "It's Mee!"
		case "/help":
			return "Help..."
		case "/search":
			return Google(msg[1:])
		default:
			return ""
		}
	}

	return ""
}
