package cmd

import (
	"fmt"
	"github.com/rocketlaunchr/google-search"
	"log"
	"strings"
)

//Google searches somthing on google
func Google(query []string) string {

	//recover
	defer HandlePanic()

	//if the query is empty return a error
	if len(query) == 0 {
		return "[ERROR]query lenght is 0"
	}
	s := strings.Join(query, "")

	if s != "" {
		result, err := googlesearch.Search(nil, s)

		if err != nil {
			log.Println("[ERROR]", err)
			return ""
		}

		var r []string
		var res string
		for index, i := range result {

			if index >= 4 {
				break
			}

			res = fmt.Sprintf("URL: %s \nTitle: %s \nDescription: %s \n\n", i.URL, i.Title, i.Description)
			r = append(r, res)
		}
		return strings.Join(r, "")
	}
	return "[ERROR]Empty Query"
}

func HandlePanic() {
	if a := recover(); a != nil {
		log.Println("[PANIC]", a)
	}
}
