module gitea.com/arydev/atriaBot

go 1.16

require (
	github.com/NicoNex/echotron/v2 v2.4.0 // indirect
	github.com/rocketlaunchr/google-search v1.1.3 // indirect
	github.com/serpapi/google-search-results-golang v0.0.0-20200815030216-632c97dac1ab // indirect
)
